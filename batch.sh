#!/bin/bash

PROGNAME=$0

usage() {
  cat << EOF >&2
Usage: $PROGNAME [-g <reference-genome>] [-r <reference-file>] [-l <locus/loci>] [-o <output-folder>] [-i <file1> <file2> etc.]

  -r <reference>: (Required) Reference genome (FASTA file)
  -o <output>:    (Required) Output folder
  -l <locus>:     (Optional) Locus ID to target (or loci as CSV string)
  -g <genome>:    (Optional) Name of the reference genome (hs1, hg19 or hg38; default: hg38)
  -a <analysis>:  (Optional) Analysis type (standard or extended; default: standard)
  -s <sex>:       (Optional) Sex of the sample
  -c <custom>:    (Optional) Custom loci file path (BED file)
  -f <logflags>:  (Optional) Name of a file that will be saved into specified output folder (e.g. flags.txt)
  -n <config>:    (Optional) File path of the config file
  -i <input>:     (Required) Input file(s) (indexed BAM or CRAM, e.g.: *.bam or: Sample001.bam Sample002.bam); must be the last parameter
EOF
  exit 1
}

# Set empty optional parameters
genome=""
analysis=""
sex=""
custom=""
logflags=""
config=""

# Parameters
while getopts r:g:o:l:a:s:c:f:n:i: o; do
  case $o in
    (r) reference=$OPTARG;;
    (g) genome=$OPTARG;;
    (o) output=$OPTARG;;
    (l) locus=$OPTARG;;
    (a) analysis=$OPTARG;;
    (s) sex=$OPTARG;;
    (c) custom=$OPTARG;;
    (f) logflags=$OPTARG;;
    (n) config=$OPTARG;;
    (i) input=$OPTARG;;
    (*) usage
  esac
done
shift "$((OPTIND - 2))"

optionalParams=""
[ -n "$genome" ] && optionalParams="--genome ${genome}"
[ -n "$locus" ] && optionalParams="${optionalParams} --locus ${locus}"
[ -n "$analysis" ] && optionalParams="${optionalParams} --analysis ${analysis}"
[ -n "$custom" ] && optionalParams="${optionalParams} --custom ${custom}"
[ -n "$sex" ] && optionalParams="${optionalParams} --sex ${sex}"
[ -n "$logflags" ] && optionalParams="${optionalParams} --logflags ${logflags}"
[ -n "$config" ] && optionalParams="${optionalParams} --config ${config}"

if [ ! -f "$reference" ]; then
    echo "Warning: Could not find/access file: ${reference}"
fi

if [ ! -d "$output" ]; then
    echo "Warning: Could not find/access folder: ${output}"
fi

if [ ! -z "$config"  -a  ! -f "$config" ]; then
    echo "Warning: Could not find/access file: ${config}"
fi

if [ ! -z "$custom"  -a  ! -f "$custom" ]; then
    echo "Warning: Could not find/access file: ${custom}"
fi

if [ ! -z "$logflags"  -a  ! -f "$logflags" ]; then
    echo "Warning: Could not find/access file: ${logflags}"
fi

for f in "$@"
do
  if [ -f "$f" ]
  then
    python3 stri.py \
		--input $f \
		--reference $reference \
		--output $output \
    ${optionalParams}
  else
    echo "Warning: Could not find/access file: ${f}"
  fi
done