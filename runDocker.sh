#!/bin/bash

PROGNAME=$0

usage() {
  cat << EOF >&2
Usage: $PROGNAME [-g <reference-genome>] [-r <reference-file>] [-l <locus/loci>] [-o <output-folder>] [-i <file1> <file2> etc.]

  -r <reference>: (Required) Reference genome (FASTA file)
  -o <output>:    (Required) Output folder
  -l <locus>:     (Optional) Locus ID to target (or loci as CSV string)
  -g <genome>:    (Optional) Name of the reference genome (hs1, hg19 or hg38; default: hg38)
  -a <analysis>:  (Optional) Analysis type (standard or extended; default: standard)
  -s <sex>:       (Optional) Sex of the sample
  -c <custom>:    (Optional) Custom loci file path (BED file)
  -f <logflags>:  (Optional) Name of a file that will be saved into specified output folder (e.g. flags.txt)
  -n <config>:    (Optional) File path of the config file
  -i <input>:     (Required) Input file(s) (indexed BAM or CRAM, e.g.: *.bam or: Sample001.bam Sample002.bam). Put in quotes when using wildcards.
EOF
  exit 1
}

# Set empty optional parameters
genome=""
analysis=""
sex=""
custom=""
logflags=""
config=""

# Parameters
while getopts "r:g:o:l:a:s:c:f:n:i:" o; do
  case "${o}" in
    (r) reference=$(realpath ${OPTARG});;
    (g) genome=${OPTARG};;
    (o) output=$(realpath ${OPTARG});;
    (l) locus=${OPTARG};;
    (a) analysis=${OPTARG};;
    (s) sex=${OPTARG};;
    (c) custom=$(realpath ${OPTARG});;
    (f) logflags=$(realpath ${OPTARG});;
    (n) config=$(realpath $OPTARG);;
    (i) input=$(realpath ${OPTARG});;
    (*) usage
  esac
done
shift "$((OPTIND - 1))"

# Check if docker image exists. If not, then download and build one.
if [[ "$(docker images -q stripy:v2.5 2> /dev/null)" == "" ]]; then
  echo "Docker image was not found. Trying to install..."
  docker build -t stripy:v2.5 https://stripy.org/Dockerfile
else
  # Run docker image and execute analysis
  echo "Docker image was found. Starting analysis..."
  
  # Path to required files for docker volumes
  ref_path="$(dirname "${reference}")"
  input_folder="$(dirname "${input}")"
  if [ -f $custom ]; then custom_folder="$(dirname "${custom}")"; else custom_folder=""; fi
  
  # Filenames
  ref_file="$(basename "${reference}")"
  input_file="$(basename "${input}")"
  [ -n "$custom" ] && custom_file="$(basename "${custom}")"
  
  # Optional parameters
  optionalParams=""
  if [ ! -n "$genome" ]; then optionalParams="-g hg38"; else optionalParams="-g ${genome}"; fi
  [ -n "$analysis" ] && optionalParams="${optionalParams} -a ${analysis}"
  [ -n "$locus" ] && optionalParams="${optionalParams} -l ${locus}"
  [ -n "$custom" ] && optionalParams="${optionalParams} -c /mnt/custom/${custom_file}"
  [ -n "$sex" ] && optionalParams="${optionalParams} -s ${sex}"
  [ -n "$logflags" ] && optionalParams="${optionalParams} -f {$output}/${logflags}"
  [ -n "$config" ] && optionalParams="${optionalParams} -n ${config}"
  if [ -f $custom ]; then custom_vol="-v ${custom_folder}:${custom_folder}"; else custom_vol=""; fi

  # Custom volume
  if [ -n "$custom" ]; then custom_vol="-v ${custom_folder}:/mnt/custom"; else custom_vol=""; fi

  # Run the container
	docker run --rm \
    -v ${ref_path}:/mnt/ref \
    -v ${output}:/mnt/results \
    -v ${input_folder}:/mnt/data \
    ${custom_vol} \
    stripy:v2.5 bash -c "./batch.sh \
                          -o /mnt/results \
                          -r /mnt/ref/${ref_file} \
                          ${optionalParams} \
                          -i /mnt/data/${input_file}"
fi
